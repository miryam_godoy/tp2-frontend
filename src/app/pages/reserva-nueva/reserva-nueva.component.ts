import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { ServicioService } from 'src/app/services/servicio.service';
import { DatePipe } from '@angular/common';
import { PacienteModel } from 'src/app/models/paciente.model';
import { HorarioAgendaModel } from 'src/app/models/agenda.model';

@Component({
  selector: 'app-reserva-nueva',
  templateUrl: './reserva-nueva.component.html',
  styleUrls: ['./reserva-nueva.component.scss'],
})
export class ReservaNuevaComponent implements OnInit {

  empleados: UsuarioModel[] = [];
  idEmp: number;
  fecha1: Date;
  idCli: number;
  clientes: PacienteModel[] = [];
  horarioAgenda: HorarioAgendaModel[]=[];

  buscarHorarioDisponible = false;
  cargando = true;


  libre = true;
  ambos = false;

  horario = new HorarioAgendaModel();





  constructor(public actionSheetController: ActionSheetController,
    private reservaNueva: ServicioService, private datePipe: DatePipe,
    public toastController: ToastController) { }

  ngOnInit() {
    /*buscar empleados */
    this.reservaNueva.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
      console.log(resp);
    });

    /* buscar pacientes */
    this.reservaNueva.getClientes()
    .subscribe( resp => {
      this.clientes = resp;

    });
  }

  /* convertir fecha */
 transformDate(date) {
  return this.datePipe.transform(date, 'yyyyMMdd');
}

async presentToast() {
  const toast = await this.toastController.create({
    message: 'Profesional o fecha no seleccionados.',
    duration: 2000
  });
  toast.present();
}

async presentToast2() {
  const toast = await this.toastController.create({
    message: 'Paciente no seleccionado.',
    duration: 2000
  });
  toast.present();
}

async presentToast3() {
  const toast = await this.toastController.create({
    message: 'Reserva creada exitosamente!.',
    duration: 2000
  });
  toast.present();
}

mostrarAmbos(){
  this.ambos = true;
  this.libre = false;
}

mostrarLibre(){
  this.ambos = false;
  this.libre = true;
}

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Buscar lugares',
      buttons: [{
        text: 'Libres y ocupados',
        //role: 'destructive',
        icon: 'heart-dislike',
        handler: () => {
          this.mostrarAmbos();
          //this.limpiarPantalla();
          //console.log("mostrar fisio", !this.pacienteBuscar, this.empBuscar);
          console.log('Delete clicked');
        }
      }, {
        text: 'Solo libres',
        icon: 'heart',
        handler: () => {
          this.mostrarLibre();
          //this.limpiarPantalla();
          console.log('Share clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          this.close();
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  close(){
    this.limpiar();
  }

  /* para saber si buscar libre o ocupado */
  buscarQue(){
    if(this.libre){
      this.buscar();
    }else if( this.ambos){
      
      this.buscarLibre();
    }
  }
 /* para buscar agenda disponible de un empleado */
  buscar(){
    if(this.idEmp !== undefined || this.fecha1 !== undefined ){
      let fecha = this.transformDate(this.fecha1);
      this.buscarHorarioDisponible = true;
      console.log(this.idEmp);
      this.reservaNueva.buscarHorarioAgendaDisponinle(this.idEmp, fecha)
      .subscribe(resp =>{
         this.horarioAgenda = resp;
          console.log("desde reserva",resp );
          this.cargando = false;
      });
    }else{
      this.presentToast();
      console.log("Seleccionar profecional primero");
    }
    
    
  }

  /* buscar ocupados y libres */
  buscarLibre(){
    if(this.idEmp !== undefined || this.fecha1 !== undefined){
      let fecha = this.transformDate(this.fecha1);
      this.buscarHorarioDisponible = true;
      console.log(this.idEmp);
      this.reservaNueva.buscarHorarioAgenda(this.idEmp, fecha)
      .subscribe(resp =>{
         this.horarioAgenda = resp;
          console.log("desde reserva",resp );
          this.cargando = false;
      });
    }else{
      this.presentToast();
      console.log("Seleccionar profecional primero");
    }

  }



  reservar(item){
    let reser = new HorarioAgendaModel();
    reser = this.horarioAgenda[item];
    this.horarioAgenda.splice( item, 1);
    console.log(reser.fechaCadena);
    if(this.idCli !== undefined ){
      this.reservaNueva.reservarTurno(this.idCli, this.idEmp, reser.fechaCadena, reser.horaInicioCadena, 
        reser.horaFinCadena )
        .subscribe(resp =>{
          console.log(resp);
          this.presentToast3();
        });
    }else{
      this.presentToast2();
      console.log("paciente no selccionado");
    }
    
  }

  limpiar(){
    this.buscarHorarioDisponible = false;
    this.idCli = undefined;
    this.idEmp = undefined;
    this.fecha1 = undefined;
  }

}
