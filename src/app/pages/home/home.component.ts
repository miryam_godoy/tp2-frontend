import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(
    public auth: ServicioService,
    private router: Router) { }

  ngOnInit() {}

  

  salir(){

    this.auth.logout();
    this.router.navigateByUrl('/login');

  }

}


