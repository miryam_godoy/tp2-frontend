import { Component, OnInit } from '@angular/core';
import { PacienteModel } from 'src/app/models/paciente.model';
import { ServicioService } from 'src/app/services/servicio.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss'],
})
export class ClientesComponent implements OnInit {

  clientes: PacienteModel[]=[];
  tempClientes: PacienteModel[]=[];
  page:number = 1;

  nombrePaciente: string;

  public toggled: boolean = false;
  

  constructor(private paciente: ServicioService) {
    this.toggled = false;
   }

  ngOnInit() {
    /* para mostrar clientes en la tabla */
    this.paciente.getClientes()
        .subscribe( resp => {
          
          this.clientes = resp;
          this.tempClientes = resp;
          
        });
       
  }

  toggle(): void {
    this.toggled = !this.toggled;
 }

 closeSearch(){
   this.toggled = false;
   this.clientes = this.tempClientes;
   this.ngOnInit();
 }

 /* buscar cliente */
 buscarCliente(){
  
 this.paciente.buscarPaciente(this.nombrePaciente)
    .subscribe( resp => { 
      this.clientes = resp;
      
     // console.log(this.clientes);
      
    });
}

guardarPaciente(i){
  let peticion = true;
  let pacienteGuardar  = new PacienteModel();
  pacienteGuardar= this.clientes[i];
  
  console.log("peticion",peticion);
  console.log("id",pacienteGuardar.idPersona);

  this.paciente.setPacienteId(pacienteGuardar.idPersona);
  this.paciente.setPeticion(true);

}

peticionNueva(){
  this.paciente.setPeticion(false);
}

}
