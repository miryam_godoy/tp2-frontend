import { Component, OnInit } from '@angular/core';
import { FichaArchivoModel } from 'src/app/models/fichaArchivo.model';
import { ServicioService } from 'src/app/services/servicio.service';
import Swal from 'sweetalert2';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-ficha-archivo',
  templateUrl: './ficha-archivo.component.html',
  styleUrls: ['./ficha-archivo.component.scss'],
})
export class FichaArchivoComponent implements OnInit {



 

  
  idFichaClinica: number;
  fichaArchivo: FichaArchivoModel[] = [];

  cargando = true;
  nombre;

  uploadedFiles: Array<File>;
  //uploadedFiles: File;
  constructor( private archivo: ServicioService,
              public toastController: ToastController) { 
    
  }

  

  ngOnInit() {

    if(this.archivo.getIdFichaArchivo() === undefined){

      this.presentToastWithOptions();

    }

    this.idFichaClinica = this.archivo.getIdFichaArchivo();
    console.log(this.idFichaClinica);

    this.archivo.mostrarFichaArchivo( this.idFichaClinica)
    .subscribe( resp => {
        
      this.fichaArchivo = resp;
      
      this.cargando = false;
      
    });
    
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      message: 'Para poder adjuntar un archivo debe crear una ficha',
      duration: 2000
    });
    toast.present();
  
  }


  onUpload(){

   let formData = new FormData();

   
  /*for (const file of this.uploadedFiles) {
    
    formData.append("idFichaClinica",this.idFichaClinica.toString() );
    formData.append(file.name, file);
  }*/
   
 
   for(let i = 0; i < this.uploadedFiles.length; i++){

    this.nombre = this.uploadedFiles[i].name;

    formData.append('file', this.uploadedFiles[i]);
    formData.append('nombre', this.nombre);
    formData.append('idFichaClinica', this.idFichaClinica.toString());
    //formData.append('Content-Type', 'application/json');
    
    
    
   }

   console.log(formData);
   console.log(this.nombre);

   //llamar al servidor

   this.archivo.setArchivo(formData, this.nombre, this.idFichaClinica)
   .subscribe(resp=>{
     console.log(resp);
   });

   this.ngOnInit();


  }

  onFileChange(e){

    this.uploadedFiles = e.target.files;
    //this.nombre = this.uploadedFiles.name
    //let formData = new FormData();
    for(let i = 0; i < this.uploadedFiles.length; i++){

      this.nombre = this.uploadedFiles[i].name;
    }

  }


eliminar( item:number){
    //console.log(this.clientes[item]);
    /* borrar de tempCliente y clientes los elementos */
    
    let index = 0;
    
    let temp = new FichaArchivoModel();
    temp =this.fichaArchivo[item];

     let j = temp.idFichaArchivo;
     //console.log(temp.idPersona);
     

    Swal.fire({
      title: 'Esta seguro?',
      text: `Seguro que desea elimiar el archivo
      ${ temp.nombre }`,
      //type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {

      if ( resp.value ){

        this.fichaArchivo.splice( item, 1);
        /*for( var i = 0; i< this.tempC.length; i++){
          if (this.tempClientes[i].nombreCompleto === temp.nombreCompleto &&
            this.tempClientes[i].email === temp.email &&
            this.tempClientes[i].telefono === temp.telefono &&
            this.tempClientes[i].tipoCliente === temp.tipoCliente){
              this.tempClientes.splice(i,1);
              break;
            }
        }*/

        this.archivo.eliminarArchivoFicha(temp.idFichaArchivo).subscribe();
        



      }

    });
    
  }



}
