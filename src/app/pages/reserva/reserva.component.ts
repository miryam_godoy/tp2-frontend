import { Component, OnInit } from '@angular/core';
import { ReservaModel } from 'src/app/models/reserva.model';
import { ServicioService } from 'src/app/services/servicio.service';
import { DatePipe } from '@angular/common';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { PacienteModel } from 'src/app/models/paciente.model';
import { Router } from '@angular/router';
import { PopoverController, ActionSheetController } from '@ionic/angular';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.scss'],
})
export class ReservaComponent implements OnInit {

  reservas: ReservaModel[] = [];
  tempReserva: ReservaModel[] = [];
  cargando = true;
  page = 1;

  empleados: UsuarioModel[] = [];
  clientes: PacienteModel[] = [];

  idEmp: number;
  idCli: number;
  fecha1: Date;
  fecha2: Date;

  pacienteBuscar = false;
  empBuscar = false;

  toggled: boolean = false;

  constructor(private reserv: ServicioService,
     private datePipe: DatePipe, private router: Router,
     public popoverController: PopoverController,
     public actionSheetController: ActionSheetController) { 
      this.toggled = false;
     
     }

  ngOnInit() {
    /* mostrar reservar */
    this.reserv.mostrarReservas()
    .subscribe(resp =>{
      this.reservas = resp;
      //console.log(resp);
      this.cargando = false;

      /* ordenar array para mostrar fechas actuales */
      this.reservas.sort((val1, val2)=> {return new Date(val2.fecha).getDate() - new 
        Date(val1.fecha).getDate()});

        this.tempReserva = this.reservas;

    });

    

    /*buscar empleados */
    this.reserv.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
      console.log(resp);
    });

    /* buscar pacientes */
    this.reserv.getClientes()
    .subscribe( resp => {
      this.clientes = resp;

    });
  }

  /*async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: ReservaComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }*/

  /*mostrarBarraEmp(){
    this.empBuscar = !this.empBuscar;
    this.pacienteBuscar = !this.pacienteBuscar;

  }*/
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Buscar reserva',
      buttons: [{
        text: 'Por fisioterapeuta',
        //role: 'destructive',
        icon: 'medkit',
        handler: () => {
          this.mostrarBarraEmp()
          this.limpiarPantalla();
          console.log("mostrar fisio", !this.pacienteBuscar, this.empBuscar);
          console.log('Delete clicked');
        }
      }, {
        text: 'Por paciente',
        icon: 'contacts',
        handler: () => {
          this.mostrarBarraPaciente();
          this.limpiarPantalla();
          console.log('Share clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          this.close();
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  toggle(): void {
    this.toggled = !this.toggled;
 }
 close(){
  this.toggled = false;
  this.limpiarPantalla();
  this.noMostrarBarra();
}

/* convertir fecha */
transformDate(date) {
  return this.datePipe.transform(date, 'yyyyMMdd');
}

buscar(){

  console.log(this.transformDate(this.fecha1));

  
  if( this.idEmp !== undefined && this.transformDate(this.fecha1) !== null && this.transformDate(this.fecha2)  !== null ){
    console.log('pedi fecha');
    this.reserv.getReservaEmp(this.idEmp, 
      this.transformDate(this.fecha1), this.transformDate(this.fecha2))
  .subscribe( resp => {
    this.reservas = resp;
  });

  }else if( this.idCli !== undefined ){
    console.log('pedi pacientes');
    this.reserv.getReservaPaciente(this.idCli)
    .subscribe(resp => {
      this.reservas = resp;
      //console.log(this.fichas);
    });

  }
  
  

}

/* limpiar todo */
limpiarPantalla(){
     
  
  this.reservas = this.tempReserva;
  this.fecha1 = undefined;
  this.fecha2 = undefined;
  this.idCli = undefined;
  this.idEmp = undefined;
  
  

}

mostrarBarraPaciente(){
  this.pacienteBuscar = true;
  this.empBuscar = false;
  
}

noMostrarBarra(){
  this.pacienteBuscar = false;
  this.empBuscar = false;
}

mostrarBarraEmp(){
  this.empBuscar = true;
  this.pacienteBuscar = false;

}

eliminarReserva(i){
  let temp = new ReservaModel();
  temp = this.reservas[i];
  
  Swal.fire({
    title: 'Esta seguro?',
    text: `Esta seguro que desea eliminar la reserva`,
    //type: 'question',
    showConfirmButton: true,
    showCancelButton: true
  }).then(resp => {

    if ( resp.value ){

      this.reserv.deleteReserva(temp.idReserva).subscribe();
      this.reservas.splice(i,1);
      this.tempReserva.splice(i,1);


      

    }

  });
  
}

guardarIdResrva(item){
  let temp = new ReservaModel();
  temp = this.reservas[item];
  this.reserv.saveIdReserva(temp.idReserva);
}


}
