import { Component, OnInit } from '@angular/core';
import { FichaClinicaModel } from 'src/app/models/ficha.model';
import { ServicioService } from 'src/app/services/servicio.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { PacienteModel } from 'src/app/models/paciente.model';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { ActionSheetController } from '@ionic/angular';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-fichas',
  templateUrl: './fichas.component.html',
  styleUrls: ['./fichas.component.scss'],
})
export class FichasComponent implements OnInit {
  page = 1

  toggled: boolean = false;
  cargando = true;

  fichas: FichaClinicaModel[] = [];
  tempFichas: FichaClinicaModel[] = [];

  empleados: UsuarioModel[] = [];
  clientes: PacienteModel[] = [];
  categorias: CategoriaModel[] = [];
  subcategorias: SubcategoriaModel[] = [];

  idEmp: number;
  idCli: number;
  idCategoria: number;
  idSubcat: number;
  fecha1: Date;
  fecha2: Date;

  /* para mostrar empleados */
  empBuscar = false;
  pacienteBuscar = false;
  fechaBuscar = false;
  subBuscar = false;


  constructor(private fichaClinica: ServicioService,
    private router: Router, private datePipe: DatePipe,
    public actionSheetController: ActionSheetController) {
      this.toggled = false;
     }

  ngOnInit() {
    /* buscar los servicios existentes*/
    this.fichaClinica.getFichas()
    .subscribe( resp => {
        
      this.fichas = resp;
      this.tempFichas = resp;
      this.cargando = false;
      
    });

    /*buscar empleados */
    this.fichaClinica.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
      console.log(resp);
    });

    /* buscar pacientes */
    this.fichaClinica.getClientes()
    .subscribe( resp => {
      this.clientes = resp;

    });

    /* mostrar las categorias */
    this.fichaClinica.getCategoria()
    .subscribe( resp => {
        
      this.categorias = resp;
      
    });
  }

  /* para menu buscar */
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Buscar ficha',
      buttons: [{
        text: 'Por fisioterapeuta',
        //role: 'destructive',
        icon: 'medkit',
        handler: () => {
        
         this.mostrarBarraEmp();
          this.limpiarPantalla();
          console.log("mostrar fisio", !this.pacienteBuscar, this.empBuscar);
          console.log('fisio clicked');
        }
      }, {
        text: 'Por paciente',
        icon: 'contacts',
        handler: () => {
          this.mostrarBarraPaciente();
          this.limpiarPantalla();
          console.log('paciente clicked');
        }
      }, {
        text: 'Por fechas',
        icon: 'calendar',
        handler: () => {
          this.mostrarBarraFecha();
          this.limpiarPantalla();
          console.log('fechas clicked');
        }
      },{
        text: 'Tipo producto',
        icon: 'logo-snapchat',
        handler: () => {
          this.mostrarBarraTipo();
          this.limpiarPantalla();
          console.log('tipo clicked');
        }
      },{
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          this.close();
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  /* mostrar empleado */
  mostrarBarraEmp(){
    this.empBuscar = true;
    this.pacienteBuscar = false;
    this.fechaBuscar = false;
    this.subBuscar = false;
  }

  /* mostrar paciente */
  mostrarBarraPaciente(){
    this.empBuscar = false;
    this.pacienteBuscar = true;
    this.fechaBuscar = false;
    this.subBuscar = false;
  }
  /* mostrar fechas */
  mostrarBarraFecha(){
    this.empBuscar = false;
    this.pacienteBuscar = false;
    this.fechaBuscar = true;
    this.subBuscar = false;
  }
  /* mostrar tipo */
  mostrarBarraTipo(){
    this.empBuscar = false;
    this.pacienteBuscar = false;
    this.fechaBuscar = false;
    this.subBuscar = true;
  }

 

  mostrarSubcat( ){
  
    /* buscar sucategorias por idCategoria */
    
      this.fichaClinica.buscarSubcategoria(this.idCategoria)
      .subscribe( resp => { 
        this.subcategorias = resp;
        console.log("aqui esta el error",this.idCategoria);
        
      });
    
  
  }

  toggle(): void {
    this.toggled = !this.toggled;
 }

 close(){
   this.toggled = false;
   this.limpiarPantalla();
  this.noMostrarBarra();
 }
 noMostrarBarra(){
  this.empBuscar = false;
  this.pacienteBuscar = false;
  this.fechaBuscar = false;
  this.subBuscar = false;
 }

 /* convertir fecha */
 transformDate(date) {
  return this.datePipe.transform(date, 'yyyyMMdd');
}

 buscar(){

  console.log(this.transformDate(this.fecha1));

  
  if( this.transformDate(this.fecha1) !== null && this.transformDate(this.fecha2)  !== null ){
    console.log('pedi fecha');
    this.fichaClinica.getFichasFechas(this.transformDate(this.fecha1), this.transformDate(this.fecha2))
  .subscribe( resp => {
    this.fichas = resp;
  });

  }else if( this.idCli !== undefined ){
    console.log('pedi pacientes');
    this.fichaClinica.getFichasPaciente(this.idCli)
    .subscribe(resp => {
      this.fichas = resp;
      console.log(this.fichas);
    });

  }else if( this.idEmp !== undefined){
    console.log('pedi empleados');
    this.fichaClinica.getFichasEmpleados(this.idEmp)
    .subscribe(resp => {
      this.fichas = resp;
      console.log(this.fichas);
    });
  }else if( this.idSubcat !== undefined && this.idCategoria !== null){
    console.log('pedi tipoProducto', this.idSubcat);
    console.log("subCatid",this.idSubcat);
    this.fichaClinica.getFichasSubcategoria(this.idSubcat)
    .subscribe(resp => {
      this.fichas = resp;
      console.log("probando desde tipo producto",this.fichas);
    });
  
  }
  
  

}

/* limpiar todo */
limpiarPantalla(){
     
  //this.ngOnInit();
  this.fichas = this.tempFichas;
  this.fecha1 = undefined;
  this.fecha2 = undefined;
  this.idCli = undefined;
  this.idEmp = undefined;
  this.idCategoria = null;
  this.idSubcat = undefined;
  

}

idFicha: number;

guardarSelectId(item){
  let temp = new FichaClinicaModel();
      temp =this.fichas[item];
      this.idFicha = temp.idFichaClinica;
      this.fichaClinica.setIdFichaArchivo(this.idFicha);
      console.log(this.idFicha);

      

}

mostrarEmp = true;
mostrarCli = false;

mostrarClipant(){
  this.mostrarCli= !this.mostrarCli;
  this.mostrarEmp = !this.mostrarEmp;
}

/* para eliminar */
borrar(i){
    
  let temp = new FichaClinicaModel();
  temp = this.fichas[i];
  
  Swal.fire({
    title: 'Esta seguro?',
    text: `Esta seguro que desea eliminar la ficha`,
    //type: 'question',
    showConfirmButton: true,
    showCancelButton: true
  }).then(resp => {

    if ( resp.value ){

      this.fichaClinica.eliminarFicha(temp.idFichaClinica).subscribe();
      this.fichas.splice(i, 1);

      

    }

  });
  
}

}
