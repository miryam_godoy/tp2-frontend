import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { ServicioService } from 'src/app/services/servicio.service';
import Swal from 'sweetalert2';
import { PacienteModel } from 'src/app/models/paciente.model';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-paciente-nuevo',
  templateUrl: './paciente-nuevo.component.html',
  styleUrls: ['./paciente-nuevo.component.scss'],
})
export class PacienteNuevoComponent implements OnInit {

  pacienteAct: PacienteModel;
  peticion: boolean = false;

  temp = new PacienteModel();

  idPaciente;
  cedula:string;
  ruc: string;
  telefono:string;
  tipoPersona: string;
  nombre: string;
  apellido: string;
  fechaNacimiento: Date;
  email: string;



  constructor(private paciente: ServicioService,
              public alertController: AlertController,
              public loadingController: LoadingController,
              private datePipe: DatePipe) { }



  ngOnInit() {

    this.buscarPeticion();
  }

  async presentAlert() {
    let alert = await this.alertController.create({
      header: 'Paciente Nuevo',
      message: 'Se ha creado un nuevo paciente con exito!!',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: "circles",
      duration: 5000,
      message: 'Guardando...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
  }

  /* crear un nuevo paciente */
  crearPaciente(cedula:string, ruc: string, telefono:string, tipoPersona: string
    ,nombre: string, apellido: string, fechaNacimiento: Date, email: string){
    //let fecha = this.transformDate(this.fechaNacimiento);
      // convertir fecha a string
    /*  let fecha = this.transformDate(fechaNacimiento);
      console.log(fecha);

      let personaTipoUpper = tipoPersona.toLocaleUpperCase();
      this.presentLoadingWithOptions();
      this.paciente.setPaciente( 
        cedula, ruc, telefono, personaTipoUpper
        ,nombre, apellido,fecha, email).subscribe(resp =>{
           console.log(resp);
           this.presentAlert();
    });

    /* comprobar si es agregar o actualizar el dato */
    let fecha = this.transformDate(fechaNacimiento);
    console.log("peticion", !this.peticion);

    if(!this.peticion){

      this.paciente.setPaciente( 
        cedula, ruc, telefono, tipoPersona
        ,nombre, apellido, fecha, email).subscribe(resp =>{
           console.log(resp);
           Swal.fire({
            title: 'El nuevo pacinte: ' + nombre,
            text: 'Se guardo correctamente',
            //type: 'success'
          });  
        });

    }else{
      console.log("actualizacion");
      //this.idPaciente = this.pacienteAct.idPersona;
      this.paciente.actualizarPaciente(this.idPaciente,cedula, ruc, telefono, tipoPersona
        ,nombre, apellido, fecha, email).subscribe(resp =>{
          console.log(resp);
          Swal.fire({
           title: 'El paciente: ' + nombre,
           text: 'Se actualizo correctamente',
           //type: 'success'
         });  
       });

    }


   this.limpiarPantall();
   


  }


  buscarPeticion(){
     this.idPaciente = this.paciente.getPacienteId();
     this.peticion = this.paciente.getPeticion();
     console.log("en crear",this.idPaciente);

     //let temp = new PacienteModel();

     

     
   if( this.peticion ){

    this.paciente.mostrarCliente(this.idPaciente)
      .subscribe( resp => {
       console.log("resp:",resp);
       this.temp = resp;
       console.log("en temp", this.temp.nombreCompleto);

       this.nombre = this.temp.nombre;
       console.log('nombre',this.nombre);
       this.apellido = this.temp.apellido;
       this.cedula = this.temp.cedula;
       this.email = this.temp.email;
       this.ruc = this.temp.ruc;
       this.telefono = this.temp.telefono;
       this.tipoPersona = this.temp.tipoPersona;
       this.fechaNacimiento = this.temp.fechaNacimiento;
 
       
       });

   }
      
       

     
     //this.crearPaciente();
  }

  limpiarPantall(){
      this.nombre = undefined;
       this.apellido = undefined;
       this.cedula = undefined;
       this.email = undefined;
       this.ruc = undefined;
       this.telefono = undefined;
       this.tipoPersona = undefined;
       this.fechaNacimiento = undefined;

       console.log("nombre", this.nombre);
  }

  /* eliminar un paciente */
  /* para eliminar */
  borrar(){
    
    Swal.fire({
      title: 'Esta seguro?',
      text: `Esta seguro que desea borrar a ${ this.temp.nombreCompleto }`,
      //type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {

      if ( resp.value ){

        this.paciente.borrarCliente(this.temp.idPersona).subscribe();

        

      }

    });
    
  }
 

  

}
