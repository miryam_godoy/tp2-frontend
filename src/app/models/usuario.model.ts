export class UsuarioModel{
    idPersona: number;
    email: string;
    password: string;
    nombre: string;
    nombreCompleto: string;
}