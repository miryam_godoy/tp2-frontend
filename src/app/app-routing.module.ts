import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { PacienteNuevoComponent } from './pages/paciente-nuevo/paciente-nuevo.component';
import { FichasComponent } from './pages/fichas/fichas.component';
import { FichaNuevaComponent } from './pages/ficha-nueva/ficha-nueva.component';
import { FichaArchivoComponent } from './pages/ficha-archivo/ficha-archivo.component';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { ReservaNuevaComponent } from './pages/reserva-nueva/reserva-nueva.component';
import { ReservaActualizarComponent } from './pages/reserva-actualizar/reserva-actualizar.component';
import { FichaActualizarComponent } from './pages/ficha-actualizar/ficha-actualizar.component';

const routes: Routes = [
  { path: 'home'    , component: HomeComponent, canActivate: [ AuthGuard ] },
  { path: 'home/pacientes'    , component: ClientesComponent, canActivate: [ AuthGuard ] },
  { path: 'paciente-nuevo'    , component: PacienteNuevoComponent, canActivate: [ AuthGuard ] },
  { path: 'home/fichas'    , component: FichasComponent, canActivate: [ AuthGuard ] },
  { path: 'ficha-nueva'    , component: FichaNuevaComponent, canActivate: [ AuthGuard ] },
  { path: 'ficha-archivo'    , component: FichaArchivoComponent, canActivate: [ AuthGuard ] },
  { path: 'home/reserva'    , component: ReservaComponent, canActivate: [ AuthGuard ] },
  { path: 'reserva-nueva'    , component: ReservaNuevaComponent, canActivate: [ AuthGuard ] },
  { path: 'reserva/actualizar'    , component: ReservaActualizarComponent, canActivate: [ AuthGuard ] },
  { path: 'ficha/actualizar'    , component: FichaActualizarComponent, canActivate: [ AuthGuard ] },







  //{ path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'login'   , component: LoginComponent },
  { path: '**', redirectTo: 'login' },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'tab1',
    loadChildren: () => import('./tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'tab2',
    loadChildren: () => import('./tab2/tab2.module').then( m => m.Tab2PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
