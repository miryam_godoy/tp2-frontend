import { UsuarioModel } from './usuario.model';
import { PacienteModel } from './paciente.model';
import { ReservaModel } from './reserva.model';
import { FichaClinicaModel } from './ficha.model';

export class HorarioAgendaModel{
    idReserva: number;
    fecha: string;
    horaInicio: string;
    horaFin: string;
    fechaHoraCreacion: string;
    flagEstado: string;
    flagAsistio: string
    observacion: string;
    idFichaClinica: FichaClinicaModel;
    idLocal: number;
    idCliente: PacienteModel;
    idEmpleado: UsuarioModel;
    fechaCadena: string;
    fechaDesdeCadena: string;
    fechaHastaCadena: string;
    horaInicioCadena: string;
    horaFinCadena: string;
}