import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  usuario:string;

  constructor(public auth: ServicioService,
    private router: Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.auth.isLoggedIn;
    //this.usuario = this.auth.getUsername();
    console.log(this.usuario);
  }

  salir(){

    this.auth.logout();
    this.router.navigateByUrl('/login');

  }

}
