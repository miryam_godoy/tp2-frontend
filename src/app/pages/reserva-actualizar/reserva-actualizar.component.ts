import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ReservaModel } from 'src/app/models/reserva.model';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-reserva-actualizar',
  templateUrl: './reserva-actualizar.component.html',
  styleUrls: ['./reserva-actualizar.component.scss'],
})
export class ReservaActualizarComponent implements OnInit {

  idReserva: number
  obs: string;
  asistido: boolean;
  reserva = new ReservaModel();
  flag: string;
  constructor(private reservaUpdate: ServicioService,
    public toastController: ToastController) { }

  ngOnInit() {
   this.idReserva = this.reservaUpdate.getReservaId();
   this.reservaUpdate.traerReserva(this.idReserva)
   .subscribe(resp =>{
      this.reserva = resp;
   });

   this.obs = this.reserva.observacion;
   console.log('obs',this.obs);

  }

  siAsistio(){
    this.asistido = true;
    this.flag = "S";
  }
  noAsistio(){
    this.asistido = false;
    this.flag = "N";
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'La reserva se ha actualizado con exito!!.',
      duration: 2000
    });
    toast.present();
  }
  actualizar(obser: string){
    this.reservaUpdate.actualizarReserva(this.idReserva, obser, this.flag)
    .subscribe(resp =>{
      console.log(resp);
      this.presentToast()

    });
  }



}
