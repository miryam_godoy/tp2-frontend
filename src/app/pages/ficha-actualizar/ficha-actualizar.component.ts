import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { FichaClinicaModel } from 'src/app/models/ficha.model';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-ficha-actualizar',
  templateUrl: './ficha-actualizar.component.html',
  styleUrls: ['./ficha-actualizar.component.scss'],
})
export class FichaActualizarComponent implements OnInit {

  idFicha: number;
  obs: string;
  ficha = new FichaClinicaModel();

  constructor(private fichaUp: ServicioService,
    public toastController: ToastController) { }

  ngOnInit() {
    this.idFicha = this.fichaUp.getIdFichaArchivo();

    this.fichaUp.getFichaId(this.idFicha)
    .subscribe(resp =>{
      this.ficha = resp;
      this.obs = this.ficha.observacion;
    });

  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'La ficha se ha actualizado correctamente!!.',
      duration: 2000
    });
    toast.present();
  }

  actualizar(fobs){

    this.fichaUp.actualizarFicha(this.idFicha, fobs)
    .subscribe(resp =>{
      console.log(resp);
      this.presentToast();

    });

  }

}
