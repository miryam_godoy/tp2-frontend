import { Component, OnInit ,Input} from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { ServicioService } from 'src/app/services/servicio.service';
import { PacienteModel } from 'src/app/models/paciente.model';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { LoadingController, AlertController } from '@ionic/angular';
import { FichaClinicaModel } from 'src/app/models/ficha.model';

@Component({
  selector: 'app-ficha-nueva',
  templateUrl: './ficha-nueva.component.html',
  styleUrls: ['./ficha-nueva.component.scss'],
})
export class FichaNuevaComponent implements OnInit {

  empleados: UsuarioModel[] = [];
  clientes: PacienteModel[] = [];
  categorias: CategoriaModel[] = [];
  subcategorias: SubcategoriaModel[] = [];

  ficha: FichaClinicaModel = new FichaClinicaModel();

  idEmp: number;
  idCli: number;
  idCategoria: number;
  idSubcat: number;
  motivoConsulta: string;
  diagnostico: string;
  observacion: string;

  idFicha: number;

  peticion: boolean = false;

  constructor(private fichaNueva: ServicioService,
          public loadingController: LoadingController,
          public alertController: AlertController,) { }

  ngOnInit() {
    /*buscar empleados */
    this.fichaNueva.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
      console.log(resp);
    });

    /* buscar pacientes */
    this.fichaNueva.getClientes()
    .subscribe( resp => {
      this.clientes = resp;

    });

    /* mostrar las categorias */
    this.fichaNueva.getCategoria()
    .subscribe( resp => {
        
      this.categorias = resp;
      
    });

    

  }

  

  mostrarSubcat( ){
  
    /* buscar sucategorias por idCategoria */
      this.fichaNueva.buscarSubcategoria(this.idCategoria)
      .subscribe( resp => { 
        this.subcategorias = resp;
        console.log(this.idCategoria);
        
      });
    
  
  }

  async presentAlert() {
    let alert = await this.alertController.create({
      header: 'Ficha Clinica',
      message: 'Se ha creado una ficha clinica con exito!!',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: "circles",
      duration: 5000,
      message: 'Guardando...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  guardarFicha(){
    
    this.fichaNueva.crearFicha(this.motivoConsulta,
      this.diagnostico, this.observacion, this.idEmp, this.idCli, this.idSubcat)
      .subscribe(resp =>{
        console.log(resp);

        this.ficha = resp;
      })

      this.presentAlert()  ;

     this.fichaId();
    
  }

  fichaId(){

    this.idFicha = this.ficha.idFichaClinica;

    this.fichaNueva.setIdFichaArchivo(this.idFicha);

    console.log(this.idFicha);

  }

}
