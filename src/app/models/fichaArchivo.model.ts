export class FichaArchivoModel{
    idFichaArchivo: number;
    idFichaClinica: number;
    nombre: string;
    urlImagen: string;
    
}