import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//import { AngularFireModule } from '@angular/fire';
//import { AngularFireAuthModule } from '@angular/fire/auth';

import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule } from '@angular/forms';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { MenuComponent } from './pages/menu/menu.component';
import { HeaderComponent } from './pages/header/header.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { PacienteNuevoComponent } from './pages/paciente-nuevo/paciente-nuevo.component';
import { DatePipe } from '@angular/common';
import { FichasComponent } from './pages/fichas/fichas.component';
import { FichaNuevaComponent } from './pages/ficha-nueva/ficha-nueva.component';
import { FichaArchivoComponent } from './pages/ficha-archivo/ficha-archivo.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { ReservaNuevaComponent } from './pages/reserva-nueva/reserva-nueva.component';
import { ReservaActualizarComponent } from './pages/reserva-actualizar/reserva-actualizar.component';
import { FichaActualizarComponent } from './pages/ficha-actualizar/ficha-actualizar.component';




@NgModule({
  declarations: [AppComponent,
   HomeComponent,
   ClientesComponent,
   MenuComponent,
   HeaderComponent,
   PacienteNuevoComponent,
   FichasComponent,
   FichaNuevaComponent,
   FichaArchivoComponent,
   ReservaComponent,
   ReservaActualizarComponent,
   FichaActualizarComponent,
   ReservaNuevaComponent,
   LoginComponent],

   exports: [
     //MenuComponent,
     HeaderComponent,
   ],

  entryComponents: [],
  imports: [BrowserModule, 
  IonicModule.forRoot(),
   AppRoutingModule,
   HttpClientModule,
   NgxPaginationModule,
   FileUploadModule,
   FormsModule,],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    DatePipe,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
