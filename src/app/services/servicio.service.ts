import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';
import { map } from 'rxjs/operators';
import { PacienteModel } from '../models/paciente.model';
import { FichaClinicaModel } from '../models/ficha.model';
import { CategoriaModel } from '../models/categoria.model';
import { SubcategoriaModel } from '../models/subcategoria.model';
import { FichaArchivoModel } from '../models/fichaArchivo.model';
import { ReservaModel } from '../models/reserva.model';
import { HorarioAgendaModel } from '../models/agenda.model';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private username: string;

  private idPaciente: number;

  private peticion = false;

  private idFichaArchivo: number;

  private urlTp = 'https://gy7228.myfoscam.org:8443/stock-pwfe';

  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';

  private apiKey = 'AIzaSyC9BCBu60WdC3rZbKNIC9vDOFa-Y_V9Pd8';

  userToken: string;

 /* para la actualizacion de paciente */
 setPacienteId(idPaciente){
   this.idPaciente = idPaciente;
   

 }

 setPeticion(peticion){
   this.peticion = peticion;
 }

 getPeticion(){
   return this.peticion;
 }

 getPacienteId(){
   return this.idPaciente;
 }
 


  setIdFichaArchivo(idFicha){

    this.idFichaArchivo = idFicha;

  }
  getIdFichaArchivo(){

    return this.idFichaArchivo;

  }

  mostrarFichaArchivo( idFichaClinica){

    let params = new HttpParams();
    params = params.append('idFichaClinica', `${idFichaClinica}`);

    return this.http.get(`${this.urlTp}/fichaArchivo`,{ params: params })
      .pipe(
        map(this.crearArregloFichaArchivo),

      );

  }

  private crearArregloFichaArchivo(archivoObj) {

    const archivoFicha: FichaArchivoModel[] = [];

    if (archivoObj === null) { return []; }

    archivoObj.lista.forEach((item) => {
      
      const archivo = new FichaArchivoModel();
      
      archivo.idFichaArchivo = item.idFichaArchivo;
      archivo.idFichaClinica = item.idFichaClinica;
      archivo.nombre = item.nombre;
      archivo.urlImagen = item.urlImagen;
      
     
      archivoFicha.push(archivo);
    });

    return archivoFicha;

  }
/* guardar archivo ficha */
setArchivo(file, nombre, idFichaClinica){

  const headers = { headers: new HttpHeaders().set('Content-Type', 'multipart/form-data') };

    /* body */
    const body = {
      'file': file,
      'nombre': nombre,
      'idFichaClinica': idFichaClinica,
      
    };
    
    return this.http.post(`${this.urlTp}/fichaArchivo/archivo`, file)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );

}


eliminarArchivoFicha(idFichaArchivo){

  /* eliminar una archivo de la ficha */
  return this.http.delete(`${this.urlTp}/fichaArchivo/${idFichaArchivo}`);
  

}
  
  constructor(private http: HttpClient) { 
    this.leerToken();
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }


  logout() {

    this.loggedIn.next(false);
    localStorage.removeItem('token');

  }

  login(usuario: UsuarioModel) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    }


    return this.http.post(`${this.url}signInWithPassword?key=${this.apiKey}`, authData)
      .pipe(
        map(resp => {
          console.log('Entro al mapa del RXJS');
          this.guardarToken(resp['idToken']);
          this.loggedIn.next(true);
          this.username = authData.nombre;
          return resp;
        })
      );


  }

 

  private guardarToken(idToken: string) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds(3600);
    localStorage.setItem('expira', hoy.getTime().toString());
  }

  leerToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }


  estaAutenticado(): boolean {

    if (this.userToken.length < 2) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));

    const expiraDate = new Date();

    expiraDate.setTime(expira);

    if (expiraDate > new Date()) {
      return true;
    } else {
      return true;
    }




  }

  /* motrar pacientes*/
  getClientes() {
    return this.http.get(`${this.urlTp}/persona`)
      .pipe(
        map(this.crearArregloPaciente),

      );
  }

   /* crear arreglo paciente */

   private crearArregloPaciente(pacCatObj) {

    const pacientes: PacienteModel[] = [];

    if (pacCatObj === null) { return []; }

    pacCatObj.lista.forEach((item) => {
      
      const paciente = new PacienteModel();
      
      paciente.idPersona = item.idPersona;
      paciente.nombreCompleto = item.nombreCompleto;
      paciente.email = item.email;
      paciente.telefono = item.telefono;
      paciente.tipoCliente = item.tipoCliente;
     
      pacientes.push(paciente);
    });

    return pacientes;

  }

  /* actualizar clientes */
  actualizarPaciente(id:number,cedula, ruc, telefono, tipoPersona
    ,nombre, apellido,fechaNacimiento, email){

      const headers = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    /* body */
    const obj = {
      "idPersona": id,
      "nombre": nombre,
      "apellido": apellido,
      "email": email,
      "telefono": telefono,
      "ruc": ruc,
      "cedula": cedula,
      "tipoPersona": tipoPersona,
      "fechaNacimiento": fechaNacimiento,

    };
  

    return this.http.put(`${this.urlTp}/persona`, obj, headers)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );

    }

  /* buscar paciente */
  buscarPaciente(nombreCliente: string) {

    let params = new HttpParams();
    params = params.append('ejemplo', `{"nombre": "${nombreCliente}"}`);



    return this.http.get(`${this.urlTp}/persona`, { params: params })
      .pipe(
        map(this.crearArregloPaciente),

      );


  }

  setPaciente(cedula: string, ruc: string, telefono: string, tipoPersona: string
    , nombre: string, apellido: string, fechaNacimiento: string, email: string){

      const headers = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    /* body */
    const obj = {
      "nombre": nombre,
      "apellido": apellido,
      "email": email,
      "telefono": telefono,
      "ruc": ruc,
      "cedula": cedula,
      "tipoPersona": tipoPersona,
      "fechaNacimiento": fechaNacimiento,

    };
    
    return this.http.post(`${this.urlTp}/persona`, obj, headers)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );


  }

 /* mostrar cliente por id */
  mostrarCliente(id: number){

    return this.http.get<PacienteModel>(`${this.urlTp}/persona/${id}`)
    
  }

  /* motrar las fichas */
  getFichas() {

    return this.http.get(`${this.urlTp}/fichaClinica`)
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* buscar fichas por fechas */
  getFichasFechas(fechaDesde: string, fechaHasta: string){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"fechaDesdeCadena":"${fechaDesde}","fechaHastaCadena":"${fechaHasta}"}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* buscar fichas por paciente */
  getFichasPaciente(idCli){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idCliente":{"idPersona":${idCli}}}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* buscar fichas por empleados */

  getFichasEmpleados(idEmp){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idEmpleado":{"idPersona":${idEmp}}}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* buscar fichas por tipo de producto */
  getFichasSubcategoria(idSubcat){

    console.log('ficha servivio', idSubcat);

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idTipoProducto":{"idTipoProducto":${idSubcat}}}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* crear areglo para fichas clinicas */
  private crearArregloFicha(ficCatObj) {

    const fichas: FichaClinicaModel[] = [];

    //console.log(ficCatObj);

    if (ficCatObj === null) { return []; }

    ficCatObj.lista.forEach((item) => {
     
      const ficha = new FichaClinicaModel();
      ficha.idFichaClinica = item.idFichaClinica;
      ficha.fechaHora = item.fechaHora;
      ficha.fechaHoraCadena = item.fechaHoraCadena;
      ficha.diagnostico = item.diagnostico;
      ficha.motivoConsulta = item.motivoConsulta;
      ficha.observacion = item.observacion;
      ficha.idEmpleado = item.idEmpleado;
      ficha.idCliente = item.idCliente;
      ficha.idTipoProducto = item.idTipoProducto;
      fichas.push(ficha);
    });


    return fichas;

  }

  /* mostrar empleados */
  getEmpleado() {

    let params = new HttpParams();
    params = params.append('ejemplo', '{"soloUsuariosDelSistema":true}');


    return this.http.get(`${this.urlTp}/persona`, { params: params })
      .pipe(
        map(this.crearArregloPersona),

      );
  }

  private crearArregloPersona(personaObj) {

    const personas: UsuarioModel[] = [];

    if (personaObj === null) { return []; }

    personaObj.lista.forEach((item) => {
      
      const persona = new UsuarioModel();
      
      persona.idPersona = item.idPersona;
      persona.nombre = item.nombre;
      persona.nombreCompleto = item.nombreCompleto;
      persona.email = item.email;
      persona.password = item.usuarioLogin;
     
      personas.push(persona);
    });

    return personas;

  }

  getCategoria() {
    return this.http.get(`${this.urlTp}/categoria`)
      .pipe(
        map(this.crearArreglo),

      );
  }

  private crearArreglo(categoriasObj) {

    const categorias: CategoriaModel[] = [];

    //console.log('lo que trae del puto array');
    //console.log(categoriasObj);


    if (categoriasObj === null) { return []; }

    categoriasObj.lista.forEach((item) => {

      const categoria = new CategoriaModel();
      categoria.idCategoria = item.idCategoria;
      categoria.descripcion = item.descripcion;
      categoria.flagVisible = item.flagVisible;
      categoria.posicion = item.posicion;

      categorias.push(categoria);
    });

    return categorias;

  }
  

  /* buscar subcategoria */

  buscarSubcategoria(idCategoria: number) {

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idCategoria":{"idCategoria": ${idCategoria}}}`);



    return this.http.get(`${this.urlTp}/tipoProducto`, { params: params })
      .pipe(
        map(this.crearArregloSubcategoria),

      );
  }

  /* retornar array de subcategorias */
  private crearArregloSubcategoria(subCatObj) {

    const subcategorias: SubcategoriaModel[] = [];

    if (subCatObj === null) { return []; }

    subCatObj.lista.forEach((item) => {
      
      const sub = new SubcategoriaModel();

      sub.idTipoProducto = item.idTipoProducto
      sub.descripcion = item.descripcion;
      
      subcategorias.push(sub);
    });

    return subcategorias;

  }

  /* guardar fichas nuevas */

  crearFicha(motivoConsulta,
    diagnostico, observacionidSubcat, idEmp, idCli, idTipoProducto){
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      headers = headers.append('usuario', 'gustavo');
        const body={
           "motivoConsulta":motivoConsulta ,
           "diagnostico":diagnostico,
           "observacion":observacionidSubcat, 
           "idEmpleado":
           { "idPersona": idEmp },
           "idCliente":
           { "idPersona": idCli }, 
           "idTipoProducto": 
           { "idTipoProducto":idTipoProducto }

        };
        return this.http.post(`${this.urlTp}/fichaClinica`, body, {headers})
      .pipe(
         map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );


    }

    /* borrar clientes */
  borrarCliente(id: number) {

    return this.http.delete(`${this.urlTp}/persona/${id}`);

  }

  /* mostrar reserva */
  mostrarReservas(){

    return this.http.get(`${this.urlTp}/reserva`)
        .pipe(
          map(this.crearArregloReserva),
  
        );


  }

  crearArregloReserva(reservaObj){
    const reservas: ReservaModel[] = [];

    

    if (reservaObj === null) { return []; }

    reservaObj.lista.forEach((item) =>{
      const reserva = new ReservaModel();
      reserva.idReserva = item.idReserva;
      reserva.observacion = item.observacion;
      reserva.flagAsistio = item.flagAsistio;
      reserva.fecha = item.fecha
      reserva.horaInicio = item.horaInicio;
      reserva.horaFin = item.horaFin;
      reserva.idEmpleado = item.idEmpleado;
      reserva.idCliente = item.idCliente;
      reservas.push(reserva);

    });
    return reservas;
  }

  getReservaPaciente(idCli){
    let params = new HttpParams();
    params = params.append('ejemplo', `{"idCliente":{"idPersona":${idCli}}}`);

    return this.http.get(`${this.urlTp}/reserva`,{ params: params })
        .pipe(
          map(this.crearArregloReserva),
  
        );

  }

  getReservaEmp(idEmpleado, fechaDesde, fechaHasta){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idEmpleado":{"idPersona": ${idEmpleado}},
    "fechaDesdeCadena":"${fechaDesde}","fechaHastaCadena":"${fechaHasta}"}`);

    return this.http.get(`${this.urlTp}/reserva`, { params: params })
        .pipe(
          map(this.crearArregloReserva),
  
        );

  }

  /* buscar horarioagenda disponible*/
  buscarHorarioAgendaDisponinle(idEmp, fecha){

    let params = new HttpParams();
    params = params.append('fecha', `${fecha}`);
    params = params.append('disponible', 'S');

    console.log(params);

    return this.http.get(`${this.urlTp}/persona/${idEmp}/agenda`, { params: params })
        .pipe(
          map(this.crearArregloHorarioAgenda),
  
        );

  }
  buscarHorarioAgenda(idEmp, fecha){
    let params = new HttpParams();
    params = params.append('fecha', `${fecha}`);

    console.log(params);

    return this.http.get(`${this.urlTp}/persona/${idEmp}/agenda`, { params: params })
        .pipe(
          map(this.crearArregloHorarioAgenda),
  
        );

  }

 crearArregloHorarioAgenda(horarioObj){

    const agenda: HorarioAgendaModel[] = [];

    console.log("desde servicio",horarioObj);

    if (horarioObj === null) { return []; }

    

   horarioObj.forEach((item) =>{
    console.log("prueba",item.horaFinCadena)
    const horaDisp = new HorarioAgendaModel();
    horaDisp.idEmpleado = item.idEmpleado;
    horaDisp.fecha = item.fecha;
    horaDisp.fechaCadena = item.fechaCadena;
    horaDisp.horaInicioCadena = item.horaInicioCadena;
    horaDisp.horaFinCadena = item.horaFinCadena;
      agenda.push(horaDisp);

    });
    return agenda;
  }

  reservarTurno(idCli, idEmp, fechaCadena, horaInicioCadena, 
    horaFinCadena ){
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      headers = headers.append('usuario', 'gustavo');
        const body={
           "fechaCadena":fechaCadena ,
           "horaInicioCadena":horaInicioCadena,
           "horaFinCadena":horaFinCadena, 
           "idEmpleado":
           { "idPersona": idEmp },
           "idCliente":
           { "idPersona": idCli } 
          
        };
        return this.http.post(`${this.urlTp}/reserva`, body, {headers})
      .pipe(
         map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );


    }

    deleteReserva(idReserva){

      return this.http.delete(`${this.urlTp}/reserva/${idReserva}`);

    }

    idReservaUpdate: number

    saveIdReserva(idReserva){

      this.idReservaUpdate = idReserva;

    }

    getReservaId(){
      return this.idReservaUpdate;
    }

    traerReserva(idReserva: number){
      return this.http.get<ReservaModel>(`${this.urlTp}/reserva/${idReserva}`);
        
    }

    actualizarReserva(idReserva:number, observacion:string, marcar: string){

      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      //headers = headers.append('usuario', 'gustavo');
  
      /* body */
      const body = {
        "idReserva": idReserva, 
        "observacion":observacion,
        "flagAsistio":marcar
      };
  
      console.log(body);
    
  
      return this.http.put(`${this.urlTp}/reserva`, body, {headers})
        .pipe(
          map((resp: any) => {
  
            console.log(resp);
            return resp;
          })
        );
      
  
    }

/* mostrar ficha */

getFichaId(idFicha){

  return this.http.get<FichaClinicaModel>(`${this.urlTp}/fichaClinica/${idFicha}`);

}

/* eliminar ficha */
eliminarFicha(idFicha){

  return this.http.delete(`${this.urlTp}/fichaClinica/${idFicha}`);

}

/* actualizar Ficha */

actualizarFicha(idFicha, observacion){
  let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      headers = headers.append('usuario', 'gustavo');
  
      /* body */
      const body = {
        "idFichaClinica": idFicha, 
        "observacion": observacion
        
      };
  
      console.log(body);
    
  
      return this.http.put(`${this.urlTp}/fichaClinica`, body,{headers})
        .pipe(
          map((resp: any) => {
  
            console.log(resp);
            return resp;
          })
        );
}

/*peticionFicha = false;

setPeticionFicha(pet){
  this.peticionFicha = pet;

}
getPeticionFicha(){
  return this.peticionFicha;

}*/

}
